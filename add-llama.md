## Installing [Llama2](https://github.com/facebookresearch/llama) Models on the same instance as Stable-Diffusion

---
### Getting Started 
To get started with llama2, request a new download link for the models [here](https://ai.meta.com/resources/models-and-libraries/llama-downloads/). The download url will be valid for 24 hours...

After a few minutes, the download link for llama2 will be emailed to you. Copy the download link to your clipboard.

---

### Llama2 Server Setup

#### Install Anaconda
- Get the anaconda installer from their website by running

    ```wget https://repo.anaconda.com/archive/Anaconda3-2023.07-1-Linux-x86_64.sh```
- Give it execution powers by running 

    ```chmod +x Anaconda3-2023.07-1-Linux-x86_64.sh```
- Run the executable file 

    ```bash Anaconda3-2023.07-1-Linux-x86_64.sh``` 
- Set conda config the the base env is not activated on startup

    ```conda config --set auto_activate_base false```
- Exit the instance and login back again to use conda.  
  - ```exit``` 
  - ```make ssh``` 
#### Create a virtual environment
Name an environment, ```llama``` using

   ```conda create --name llama python=3.10```


#### Activate the environment
```conda activate llama```

#### Install PyTorch / CUDA in a Conda environment
Install pytorch with CUDA support.

```conda uninstall pytorch torchvision torchaudio pytorch-cuda=11.7 -c pytorch -c nvidia```

### Running llama2 on the instance

---
#### Install llama2
- Activate the conda environment
 
    ```conda activate llama```

- Clone the repo from github.

    ```git clone https://github.com/facebookresearch/llama.git```
- Enter the repo 

    ```cd llama```
- Install all requirements

    ```pip install -e .```
- Run the llama download script in the repo 

    ```bash download.sh```
  - when prompted, enter the email to which the download link was sent, select the llama models you want to download, and the download link 
  - when models are loaded successfully this will be the output
  ![image](llama_2_models.png)
  
#### Running llama2

- I faced errors while trying to run this command to test successful deployment

    ```torchrun --nproc_per_node 1 example_text_completion.py  --ckpt_dir llama-2-7b/  --tokenizer_path tokenizer.model --max_seq_len 128 --max_batch_size 4 ```
- This is the pytorch-related error
    ![image](pytorch-error.png)
- I would appreciate help with resolving this
- I found it's a common error when running llama on GPU [here](https://github.com/facebookresearch/llama/issues/482).