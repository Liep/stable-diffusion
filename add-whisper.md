## Installing [Whisper](https://github.com/openai/whisper) Models on the same instance as Stable-Diffusion

---
### Getting Started 
1. Using conda, create a new environment named whisper

    ```conda create --name whisper python=3.10```
2. Activate the environment

    ```conda activate whisper```
3. Install whisper into the environment

    ```pip install -U openai-whisper```
4. Install ```ffmpeg``` and ```rust```

    ```
    sudo apt install ffmpeg
    pip install setuptools-rust
    ```
5. Download an audio to test transcription

    ```wget http://www.moviesoundclips.net/movies1/edgeoftomorrow/battle.mp3```
6. Run whisper against the audio using any one of the models, e.g. ```medium```

    ```whisper battle.mp3 --model medium```
7. This was the output

    ![image](output.png)